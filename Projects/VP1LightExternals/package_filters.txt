# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of VP1LightExternals.
#
+ External/Boost
#+ External/CLHEP
+ External/Coin3D
+ External/dcap
+ External/Eigen
+ External/GeoModelCore
+ External/GeoModelIO
+ External/GoogleTest
+ External/HEPUtils
+ External/LibXml2
+ External/MCUtils
+ External/Python
+ External/ROOT
+ External/Simage
+ External/SoQt
+ External/XRootD
+ External/HepPDT
- .*
