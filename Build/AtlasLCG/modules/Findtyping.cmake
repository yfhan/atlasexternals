# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#  TYPING_PYTHON_PATH
#
# Can be steered by TYPING_LCGROOT.
#

# The LCG include(s).
include( LCGFunctions )

# Find it.
lcg_python_external_module( NAME typing
   PYTHON_NAMES typing/__init__.py typing.py )

# Handle the standard find_package arguments.
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( typing DEFAULT_MSG
   _TYPING_PYTHON_PATH )

# Set up the RPM dependency.
lcg_need_rpm( typing )
