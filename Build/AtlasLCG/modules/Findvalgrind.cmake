# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Defines:
#
#  VALGRIND_FOUND
#  VALGRIND_INCLUDE_DIR
#  VALGRIND_INCLUDE_DIRS
#  VALGRIND_BINARY_PATH
#
# Can be steered by VALGRIND_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# Declare the external module:
lcg_external_module( NAME valgrind
   INCLUDE_SUFFIXES include
   INCLUDE_NAMES valgrind/valgrind.h
   BINARY_SUFFIXES bin
   BINARY_NAMES valgrind )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( valgrind DEFAULT_MSG
   VALGRIND_INCLUDE_DIR VALGRIND_BINARY_PATH )

# Set additional environment variables:
set( VALGRIND_ENVIRONMENT
   SET VALGRIND_LIB  ${VALGRIND_LCGROOT}/lib/valgrind
   SET VALGRIND_OPTS "--smc-check=all" )

# Set up the RPM dependency:
lcg_need_rpm( valgrind )
