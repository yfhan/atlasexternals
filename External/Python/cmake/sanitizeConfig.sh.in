#!/bin/bash
#
# Script used to sanitize the python installation produced by the build,
# so that it would be relocatable.
#
# Note that all sed commands use ":" as the separator in the sed expressions,
# so that path names (which have plenty of "/" in them) would not have to be
# massaged before giving them to the command.
#

# Fail on errors:
set -e

# Loop over the executable scripts that need to be massaged:
for script in @_buildDir@/bin/python2.7-config \
              @_buildDir@/bin/2to3 \
              @_buildDir@/bin/idle \
              @_buildDir@/bin/pydoc \
              @_buildDir@/bin/smtpd.py; do

    # Create a sanitized version of the script, by just replacing
    # its first line with a relocatable expression:
    sed -i.bak -e '1s:.*:#!/usr/bin/env python:' ${script}
    # Remove the old version:
    @CMAKE_COMMAND@ -E remove ${script}.bak
done

# Create a sanitized version of _sysconfigdata.py, using sed:
sed -i.bak -e '1i\
import os; installdir=os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))' \
    @_buildDir@/lib/python2.7/_sysconfigdata.py
sed -i.bak -e "s:'@_buildDir@:installdir + ':g" \
    @_buildDir@/lib/python2.7/_sysconfigdata.py
sed -i.bak \
    -e ':loop' \
    -e "s:\('DESTDIRS'.*\)@_buildDir@:\1' + installdir + ':g" \
    -e 't loop' \
    @_buildDir@/lib/python2.7/_sysconfigdata.py
sed -i.bak \
    -e ':loop' \
    -e "s:\('INCLDIRSTOMAKE'.*\)@_buildDir@:\1' + installdir + ':g" \
    -e 't loop' \
    @_buildDir@/lib/python2.7/_sysconfigdata.py
# Remove the old version:
@CMAKE_COMMAND@ -E remove @_buildDir@/lib/python2.7/_sysconfigdata.py.bak
