#
# pip requirements file for External/PyModules
#

# Only install packages from our own pip repository:
--index-url https://atlas-software-dist-eos.web.cern.ch/atlas-software-dist-eos/externals/PyModules/repo

# List the packages and (their version) one per line:
flake8==3.7.7
